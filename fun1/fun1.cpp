#include <iostream>

#include <opencv2/opencv.hpp>

#include <QtWidgets>
#include <QtMultimedia>


class MyVideoSurface : public QAbstractVideoSurface
{
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(
            QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const
    {
        Q_UNUSED(handleType);

        // Return the formats you will support
        return QList<QVideoFrame::PixelFormat>() << QVideoFrame::Format_BGR24;
    }

    bool present(const QVideoFrame &frame)
    {
        Q_UNUSED(frame);
        // Handle the frame and do your processing
        using namespace std;
        QVideoFrame q = frame;
        int rows = q.height(), cols = frame.width();
        cout << "Frame !" << cols << " x " << rows << endl;
        q.map(QAbstractVideoBuffer::ReadOnly);
        cout << q.isMapped() << endl;

        cv::Mat m(rows, cols, CV_8UC3);
        std::memcpy(m.data, frame.bits(), rows*cols*3);
        if (count >=80 && count < 100)
            cv::imwrite("frame" + to_string(count) + ".png", m);

        ++count;
        return true;
    }

    int count=0;
};

int main(int argc, char **argv){
    QApplication app(argc, argv);
    qDebug() << "QT_VERSION (COMPILE TIME) = " << QT_VERSION_STR;
    qDebug() << "QT_VERSION (RUN TIME) = " << qVersion();

    QMediaPlayer p;
    p.setMedia(QUrl::fromLocalFile(QFileInfo("../data/tvoya.mp4").absoluteFilePath()));

    MyVideoSurface *mvs = new MyVideoSurface();
    p.setVideoOutput(mvs);

    p.play();

    return app.exec();
}
