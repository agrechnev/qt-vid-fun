#include <QtWidgets>

#include "./MainWin.h"

/// By Oleksiy Grechnyev
/// Here I load an image via cv::Mat, then convert to QPixmap and display

int main(int argc, char **argv){
    QApplication app(argc, argv);
    qDebug() << "QT_VERSION (COMPILE TIME) = " << QT_VERSION_STR;
    qDebug() << "QT_VERSION (RUN TIME) = " << qVersion();

    MainWin mainWin;
    mainWin.show();

    return app.exec();
}
