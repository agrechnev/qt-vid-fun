//
// Created by Oleksiy Grechnyev on 5/30/19.
//

#pragma once

#include <QtWidgets>

class MainWin : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(MainWin)
public:
    explicit MainWin(QWidget *parent = nullptr);

private:
    QPixmap pixmap;
    QLabel * imgPane;
};


