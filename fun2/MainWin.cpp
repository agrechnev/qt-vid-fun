//
// Created by Oleksiy Grechnyev on 5/30/19.
//

#include <iostream>

#include <opencv2/opencv.hpp>

#include "MainWin.h"

MainWin::MainWin(QWidget *parent) :
        QWidget(parent) {
    using namespace std;
    using namespace cv;
    setFixedSize(800, 600);

    // Load pixmap directly
//    pixmap.load("../data/gpig.jpg");

    // Load via cv::Mat and QImage
    Mat m0 = imread("../data/gpig.jpg");
    Mat m;
    cvtColor(m0, m, COLOR_BGR2RGB);
    pixmap.convertFromImage(QImage(m.data, m.cols, m.rows, m.cols*3 , QImage::Format_RGB888));


    cout << pixmap.width() << "x" << pixmap.height() << endl;
    imgPane = new QLabel(this);
    imgPane->setPixmap(pixmap);
}
