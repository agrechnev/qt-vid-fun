//
// Created by Oleksiy Grechnyev on 5/30/19.
//

#include <iostream>

#include "ImagePane.h"

void ImagePane::nextFrame(void *buf, int w, int h) {
    using namespace std;
//    cout << "Received Frame :" << w << " x " << h << endl;
    p.convertFromImage(QImage((uchar *)buf, w, h, w*3 , QImage::Format_RGB888));
    if (width() != w || height() != h)
        setFixedSize(w, h);
    setPixmap(p);
}

ImagePane::ImagePane(QWidget *parent) :
        QLabel(parent) {

}
