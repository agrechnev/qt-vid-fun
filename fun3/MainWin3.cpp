//
// Created by Oleksiy Grechnyev on 5/30/19.
//

#include "MainWin3.h"

MainWin3::MainWin3(QWidget *parent) :
        QWidget(parent) {
    setFixedSize(800, 600);
    imagePane = new ImagePane(this);


    // Start the media player
    player.setMedia(QUrl::fromLocalFile(QFileInfo("../data/tvoya.mp4").absoluteFilePath()));
    player.setVideoOutput(&vSurface);
    connect(&vSurface, &VSurface::nextFrame, imagePane, &ImagePane::nextFrame);
    player.play();
}
