//
// Created by Oleksiy Grechnyev on 5/30/19.
//

#pragma once


#include <QtWidgets>
#include <QtMultimedia>

#include "VSurface.h"
#include "ImagePane.h"

class MainWin3 : public QWidget {
Q_OBJECT

    Q_DISABLE_COPY(MainWin3)

public:
    explicit MainWin3(QWidget *parent = nullptr);

private:
    QMediaPlayer player;

    VSurface vSurface;
    ImagePane *imagePane;
};


